<?php

namespace App\Http\Controllers\API;

use App\Models\Category;
use App\Tools\Transformers\CategoryTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CategoriesController extends ApiController
{
    /*
     * @var Models\Category
     */
    private $model;
    /*
     * @var Tools\Transformers\CategoryTransformer
     */
    private $transformer;

    /**
     * CategoriesController constructor.
     * @param CategoryTransformer $categoryTransformer
     */
    public function __construct(CategoryTransformer $categoryTransformer)
    {
        $this->model = new Category();
        $this->transformer = $categoryTransformer;
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public function getCategories($categoryId)
    {
        try {
            return $categoryId ? $this->model->findOrFail($categoryId)->subCategories : $this->model->paginate();
        } catch (ModelNotFoundException $e) {
            return false;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param null $categoryId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($categoryId = null)
    {
        $categories = $this->getCategories($categoryId);
        if (!$categories) {
            return $this->respondWithNotFound(__('messages.categoryError404'));
        }
        return $this->setStatusMessage(__('messages.success'))
            ->respond([
                'data' => $this->transformer->transformCollection($categories->all())
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show($category = null)
    {
        $categoryDetails = $this->model->find($category);
        if (!$categoryDetails) {
            return $this->respondWithNotFound(__('messages.categoryError404'));
        }
        return $this->respond(['data' => $this->transformer->transform($categoryDetails)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }

}
