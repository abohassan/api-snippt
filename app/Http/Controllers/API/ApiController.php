<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;

class ApiController extends BaseController
{
    /*
     * @var int
     */
    protected $statusCode = 200;

    /*
     * @var string
     */
    protected $statusMessage = 'Operation done successfully';

    /**
     * @return string
     */
    public function getStatusMessage(): string
    {
        return $this->statusMessage;
    }

    /**
     * @param string $statusMessage
     * @return ApiController
     */
    public function setStatusMessage(string $statusMessage)
    {
        $this->statusMessage = $statusMessage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return ApiController
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function respondWithNotFound($message = 'Not Found!')
    {
        return $this
            ->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)
            ->respondWithError($message);
    }

    public function respond($data = [], $headers = [])
    {
        $response = array_merge([
            'status_code' => $this->getStatusCode(),
            'message' => $this->getStatusMessage()
        ], $data);
        return Response::json($response, $this->getStatusCode(), $headers);
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithError($message)
    {
        return $this->setStatusCode(404)->setStatusMessage($message)->respond();
    }
}
