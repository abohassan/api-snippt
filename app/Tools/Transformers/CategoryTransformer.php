<?php
/**
 * Created by PhpStorm.
 * User: Mohamed Abohassan
 * Date: 6/6/2018
 * Time: 4:36 AM
 */

namespace App\Tools\Transformers;


class CategoryTransformer extends Transformer
{

    public function transform($category)
    {
        return [
            'id' => $category['id'],
            'title' => $category['title'],
            'description' => $category['description'],
            'image' => $category['image'],
            'parentCategoryId' => $category['parentId']
        ];
    }
}
