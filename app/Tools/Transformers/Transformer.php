<?php
/**
 * Created by PhpStorm.
 * User: Mohamed Abohassan
 * Date: 6/6/2018
 * Time: 4:33 AM
 */

namespace App\Tools\Transformers;


abstract class Transformer
{
    /*
     * Transform a collection of items
     *
     * @param $items
     * @return array
     */
    public function transformCollection($items)
    {
        return array_map([$this, 'transform'], $items);
    }

    abstract public function transform($item);
}
