<?php

use App\Models\Category;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            Category::create([
                "title" => $faker->sentence(2),
                "description" => $faker->sentence(5),
                "image" => $faker->image('public/images/categories', 200, 200, null, false)
            ]);
        }
    }
}
