<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class DatabaseSeeder extends Seeder
{
    /*
     * @var array
     */
    private $tables = ['Categories'];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->cleanDatabase();
        $this->call(CategoriesSeeder::class);
    }

    public function cleanDatabase(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
            if($table === 'Categories') {
                File::deleteDirectory(public_path('/images/categories'));
                File::makeDirectory(public_path('/images/categories'));
            }
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
