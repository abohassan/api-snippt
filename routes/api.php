<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('API')->group(function () {
    // routes required api auth
    Route::middleware(['auth:api'])->group(function () {
        Route::resource('users', 'UsersController', ['only' => ['store', 'update', 'index']]);

        Route::resource('categories', 'CategoriesController', [
            'only' => ['store', 'update']
        ]);
    });
    Route::resource('users', 'UsersController', ['only' => ['show']]);
    Route::get('categories/{categoryId}/subCategories', 'CategoriesController@index');
    Route::resource('categories', 'CategoriesController', [
        'only' => ['index', 'show']
    ]);
});
