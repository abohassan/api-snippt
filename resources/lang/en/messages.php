<?php

return [
    'success' => 'Operation done successfully',
    'categoryError404' => 'No category with this details',
];
